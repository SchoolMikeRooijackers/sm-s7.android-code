package com.fhict.calculator.test;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.test.UiThreadTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.fhict.calculator.CalculatorActivity;
import com.fhict.calculator.R;

public class CalculatorActivityTest extends ActivityUnitTestCase<CalculatorActivity> {

    CalculatorActivity activity;
    private TextView outputView;
    private TextView inputAView;
    private TextView inputBView;
    private RadioGroup radioGroup;
	private Button button;

    public CalculatorActivityTest() {
        super(CalculatorActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                CalculatorActivity.class);
        startActivity(intent,null,null);
        activity = getActivity();

        outputView = (TextView) activity.findViewById(R.id.textViewOutput);
        inputAView = (TextView) activity.findViewById(R.id.editTextInputA);
        inputBView = (TextView) activity.findViewById(R.id.editTextInputB);
        radioGroup = (RadioGroup) activity.findViewById(R.id.radioGroup);
        button = (Button) activity.findViewById(R.id.buttonCalculate);
    }

    private void clickOnCalculateButton() {
        button.callOnClick();
    }

    private void setSomeInput() {
        inputAView.setText("3");
        inputBView.setText("4");
    }

    private void assertOutputText(String expected) {
        String actual = outputView.getText().toString();
        assertEquals(expected,actual);
    }
    
    @SmallTest
    @UiThreadTest
    public void testOutputHintIsDisplayed() {
       assertFalse(true);
    }
  
    @SmallTest
    @UiThreadTest
    public void testErrorIsDisplayedWhenNoOperationSelected() {
        assertFalse(true);
     }
    
    @SmallTest
	@UiThreadTest
	public void testWhenNoNumberAreFilledInMessageIsDisplayed() {
    	assertFalse(true);
    }
    
    @SmallTest
    @UiThreadTest
    public void testNumbersCanBeSetAndAreCalculatedWithAddAndDisplayed() throws Throwable {
    	assertFalse(true); 
    }

}
