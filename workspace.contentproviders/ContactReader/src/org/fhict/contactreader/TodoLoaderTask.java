package org.fhict.contactreader;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.os.AsyncTask;

public class TodoLoaderTask extends AsyncTask<Void, Void, List<String>> {
	private OnContactsLoadedListener listener;
	private ContentResolver contentResolver;

	public TodoLoaderTask(OnContactsLoadedListener listener,
			ContentResolver contentResolver)	{
		this.listener = listener;
		this.contentResolver = contentResolver;
	}

	@Override
	protected List<String> doInBackground(Void... arg0) {
		List<String> todos = new ArrayList<String>();

		todos.add("Not empty...");

		return todos;
	}

	@Override
	protected void onPostExecute(List<String> todos) {
		listener.onLoaded(todos);
	}
}
