Use this workspace for your work for the android course.

# Note for workspace.testing

Running this workspace with the Eclipse ADT version 4.2 might cause some issues in running the Junit4 testcases of the `CalculatorRobolectricTest` project.

The workaround is to install the latest [Eclipse standard version](https://www.eclipse.org/downloads/) and  install the [Android ADT plugin for eclipse](http://developer.android.com/tools/sdk/eclipse-adt.html), also see [installing the eclipse plugin](http://developer.android.com/sdk/installing/installing-adt.html).

## Known installation steps:


More details on setting up eclipse with robolectric in this [robolectric quick-guide](http://robolectric.org/eclipse-quick-start/). Here you will find the details on the following steps:

+ Select the correct android version ([android-18 or lower!](http://robolectric.org/eclipse-quick-start/)) for the `Calculator` and `CalculatorDeviceTest` project.
+ Make sure `CalculatorRobolectricTest` is pointing to your local android-sdk installation:  `sdk\platforms\android-18\android.jar` file.

