package com.fhict.customviewexample;

import com.fhict.customviewexample.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/* Note: created this custom ratingBar because images don't scale of android RatingBar */
public class CustomRatingBar extends RelativeLayout implements OnClickListener {
	private int rating;
	private ImageView[] images; 
	private boolean editable = false;
	private OnRatingBarClickListener listener;
	
	public CustomRatingBar(Context context, AttributeSet attrs) {
		   super(context, attrs);
		   
		   LayoutInflater inflater = (LayoutInflater)context.getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			inflater.inflate(R.layout.custom_ratingbar, this, true);
			
			images = new ImageView[] { 
					(ImageView)findViewById(R.id.ratingImage1),
					(ImageView)findViewById(R.id.ratingImage2),
					(ImageView)findViewById(R.id.ratingImage3),
					(ImageView)findViewById(R.id.ratingImage4),
					(ImageView)findViewById(R.id.ratingImage5) };
			
			for(ImageView imageView : images) {
				imageView.setOnClickListener(this);
			}
			
			TypedArray a = context.getTheme().obtainStyledAttributes(
			        attrs,
			        R.styleable.CustomRatingBar,
			        0, 0);
		   
		   boolean isEditable = a.getBoolean(R.styleable.CustomRatingBar_isEditable, true);
		   setEditable(isEditable);
	}		   

	public int getRating() {
		return this.rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
		for (int currentChildIndex = 0; currentChildIndex < images.length; currentChildIndex++) {
			ImageView currentChild = images[currentChildIndex];
			if (currentChildIndex < rating) {
				currentChild.setImageResource(R.drawable.star);
			}
			else {
				currentChild.setImageResource(R.drawable.star_not_selected);
			}
		}       
	}

	@Override
	public void onClick(View clickedView) {
		if(!editable) return;
		
		for (int currentChildIndex = 0; currentChildIndex < images.length; currentChildIndex++) {
			if (images[currentChildIndex].equals(clickedView)) {
				this.setRating(currentChildIndex + 1);
			}
		}
		
		if(listener != null) {
			listener.onClick(getRating());
		}
	}

	public int getMax() {
		return images.length;
	}
	
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	public void setOnClickListener(OnRatingBarClickListener listener) {
		this.listener = listener; 
	}
}