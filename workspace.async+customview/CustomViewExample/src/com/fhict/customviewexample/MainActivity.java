package com.fhict.customviewexample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.fhict.customviewexample.R;

public class MainActivity extends Activity {

	private CustomRatingBar ratingBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		ratingBar = (CustomRatingBar) findViewById(R.id.ratingBar);
		
		Button buttonGetRating = (Button) findViewById(R.id.buttonGetRating);
		buttonGetRating.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				displayRating(ratingBar.getRating());
			}
		});
		
		OnRatingBarClickListener ratingBarListener = new OnRatingBarClickListener() {
			@Override
			public void onClick(int rating) {
				displayRating(rating);
			}
		};

		ratingBar.setOnClickListener(ratingBarListener);
		ratingBarListener.onClick(ratingBar.getRating());
	}

	private void displayRating(int rating) {
		TextView echoText = (TextView) findViewById(R.id.ratingEchoText);
		echoText.setText("Rating is: " + rating);
	}
}
