package com.fhict.customviewexample;

public interface OnRatingBarClickListener {
	void onClick(int rating);
}
