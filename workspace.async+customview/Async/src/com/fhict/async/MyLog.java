package com.fhict.async;

import android.util.Log;

public class MyLog {
	final static String tag = "AsyncTest";
	
	public static void d(String msg) {
		Log.d(tag, msg);
	}
	
	public static void e(String msg) {
		Log.e(tag, msg);
	}
	
	public static void v(String msg) {
		Log.v(tag, msg);
	}
}
