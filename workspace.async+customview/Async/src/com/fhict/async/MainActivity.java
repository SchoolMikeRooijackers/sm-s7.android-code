package com.fhict.async;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	private AsyncTask task;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Button buttonStart = (Button) findViewById(R.id.buttonStart);
		buttonStart.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
//				task = new ShowNumberTask();
//				task.execute();

				ShowNumberTaskWithCancelAndProgressUpdate myTask = new ShowNumberTaskWithCancelAndProgressUpdate();
				myTask.execute(1L,2L,3L,4L,5L,6L);
				task = myTask;
			}
		});

		Button buttonInterrupt = (Button) findViewById(R.id.buttonInterrupt);
		buttonInterrupt.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				boolean force = true;
				task.cancel(force);
			}
		});

		Button buttonCancel = (Button) findViewById(R.id.buttonCancel);
		buttonCancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				boolean force = false;
				task.cancel(force);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	class ShowNumberTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			TextView resultText = (TextView) findViewById(R.id.textOut);
			resultText.setText("Started..");
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				Thread.sleep(2500);
			} catch (InterruptedException e) {
				return null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			TextView resultText = (TextView) findViewById(R.id.textOut);
			resultText.setText("Finished!");
		}
	}

	class ShowNumberTaskWithCancelAndProgressUpdate extends AsyncTask<Long, Long, String> {
		TextView resultText;

		@Override
		protected void onPreExecute() {
			resultText = (TextView) findViewById(R.id.textOut);
			resultText.setText("Started..");
		}

		@Override
		protected void onProgressUpdate(Long... values) {
			resultText.setText("Progress-length: " + values[0]);
		}

		/* Check out: http://stackoverflow.com/questions/11165860/asynctask-oncancelled-not-being-called-after-canceltrue
		 * for details */
		@Override
		protected void onCancelled() {
			resultText.setText("Cancelled");
		}

		@Override
		protected void onCancelled(String result) {
			resultText.setText(result);
		}

		@Override
		protected void onPostExecute(String result) {
			resultText.setText(result);
		}

		@Override
		protected String doInBackground(Long... params) {
			for(Long param : params) {
				if(isCancelled()) {
					return "Cancelled";
				}

				publishProgress(param);

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					return "Interrupted";
				}
			}

			return "Result = Ok";
		}
	}
}
