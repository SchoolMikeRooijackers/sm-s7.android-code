package com.example.fragmentplayground;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		FragmentTop fragmentTop = (FragmentTop) getFragmentManager().findFragmentById(R.id.fragment_top);
		FragmentBottom fragmentBottom = (FragmentBottom) getFragmentManager().findFragmentById(R.id.fragment_bottom);
		
		fragmentTop.setMessageListener(fragmentBottom);
	}
}
