package com.example.fragmentplayground;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FragmentTop extends Fragment {

	private MessageListener messageListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_top, container);
	}
	
	public void setMessageListener(MessageListener messageDisplayer) {
		this.messageListener = messageDisplayer;
		Button button = (Button) getView().findViewById(R.id.button1);
		
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				EditText editText = (EditText) getView().findViewById(R.id.textView1);
				String message = editText.getText().toString();
				FragmentTop.this.messageListener.onReceiveMessage(message);
			}
		});
	}

}
