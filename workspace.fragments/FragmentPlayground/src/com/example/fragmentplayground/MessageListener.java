package com.example.fragmentplayground;

public interface MessageListener {
	void onReceiveMessage(String message);
}
