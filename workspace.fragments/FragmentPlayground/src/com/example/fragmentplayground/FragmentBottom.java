package com.example.fragmentplayground;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentBottom extends Fragment implements MessageListener {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_bottom, container);
	}

	@Override
	public void onReceiveMessage(String message) {
		TextView tv = (TextView) getView().findViewById(R.id.textViewReceived);
		tv.append("+" + message);
		
	}
}
