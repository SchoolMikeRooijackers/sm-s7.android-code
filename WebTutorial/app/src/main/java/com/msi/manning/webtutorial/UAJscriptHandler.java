package com.msi.manning.webtutorial;

import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class UAJscriptHandler {
	private String tag = "UAJscriptHandler";
	private Context context = null;

	public UAJscriptHandler(Context context) {
		Log.i(tag,"script handler created");
		this.context = context;
	}
	
	private void Log(String s) {
		Log.i(tag,s);
	}
	
	@JavascriptInterface
	public void Info(String s) {
		Toast.makeText(context,s,Toast.LENGTH_LONG).show();
	}
	

	@JavascriptInterface
	public void PlaceCall(String number) {
		Log("Placing a phone call to [" + number + "]");
		String url = "tel:" + number;
		Intent callIntent = new Intent(Intent.ACTION_DIAL,Uri.parse(url));
		context.startActivity(callIntent);
	}

	@JavascriptInterface
	public void SetSearchTerm(String searchTerm) {
	
		WTApplication app = (WTApplication) context.getApplicationContext();
		app.setSearchTerm(searchTerm);
	}
	
}

