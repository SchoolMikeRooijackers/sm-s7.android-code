package com.msi.manning.webtutorial;

import android.content.Context;
import android.util.Log;
import android.graphics.Bitmap;
import android.webkit.*;
import android.webkit.WebView.FindListener;
import android.widget.Toast;

public class UAWebViewClient extends WebViewClient{

	@Override
	public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
		// TODO Auto-generated method stub
		return super.shouldInterceptRequest(view, url);
	}

	private String tag = "UAWebViewClient";
	private Context context;
	private final String MY_URL_PARAM = "/search?as_q=";
	
	public UAWebViewClient(Context context) {
		super();
		this.context = context;
	}
	
	public void onPageStarted(WebView wv,String url,Bitmap favicon) {
		super.onPageStarted(wv,url,favicon);
		Log.i(tag,"onPageStarted[" + url + "]");
		if (!url.equals(WebTutorial.STARTING_PAGE)) {
			WTApplication app = (WTApplication) context;
			String toSearch = app.getSearchTerm();
			if (toSearch != null && toSearch.trim().length() > 0) {
				Toast.makeText(context,"Searching for " + toSearch,Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	public void onPageFinished(WebView wv,String url) {
		super.onPageFinished(wv,url);
		Log.i(tag,"onPageFinished");
		if (!url.equals(WebTutorial.STARTING_PAGE)) {
			WTApplication app = (WTApplication) context;
			String toSearch = app.getSearchTerm();
			if (toSearch != null && toSearch.trim().length() > 0) {
				
				
				FindListener findListener = new FindListener() {
					
					@Override
					public void onFindResultReceived(int activeMatchOrdinal,
							int numberOfMatches, boolean isDoneCounting) {
						Log.i(tag,"term found [" + numberOfMatches + "] times.");
						Toast.makeText(context, numberOfMatches + " occurences.",Toast.LENGTH_SHORT).show();
					}
				};
				
				wv.setFindListener(findListener);
				wv.findAllAsync(app.getSearchTerm());
			}
		}
	}

	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		
		if(url.contains("google.com") && !url.contains(MY_URL_PARAM)) {
			String myUrl = url + MY_URL_PARAM + ((WTApplication)context).getSearchTerm();
			Log.i(tag,"Override url: " + myUrl);
			view.loadUrl(myUrl);
			return true;
		}
		
		Log.i(tag, "Not overriding: " + url);
		return false;
	}
	
	
	
}
